// vector2Demo.cpp : Defines the entry point for the console application.
// A deeper look at vectors and how they manage memory for you

#include "stdafx.h"
#include "Resource.h"
#include <vector>
#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	Resource r("local");
	cout << "------------------------------" << endl;

	std::vector<Resource> resources;
	//r being passed by value so a copy will be made
	resources.push_back(r);
	cout << "------------------------------" << endl;
	//we will use an anonymous temporary, the one passed in the push_back will be deleted once in the vector
	//vector will now resize by copying the elements of the old vector and then deleting the old vector
	resources.push_back(Resource("first"));
	cout << "------------------------------" << endl;
	//again resizing
	resources.push_back(Resource("second"));
	cout << "------------------------------" << endl;
	resources.push_back(Resource("third"));
	cout << "------------------------------" << endl;

	//once we go past the return, the vector will be out of scope and all the Resource objects will be destructed as well
	return 0;
}

