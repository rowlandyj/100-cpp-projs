#pragma once
#include <string>

using std::string;

class Resource
{
private:
	string name;
public:
	Resource(string n);
	Resource(const Resource& r);
	Resource& operator=(const Resource& r);
	~Resource(void);
	string getName() const {return name;}
};

