#include "StdAfx.h"
#include "Resource.h"
#include <iostream>

using std::cout;
using std::endl;


Resource::Resource(string n): name(n)
{
	cout << "constructing " << name << endl;
}

Resource::Resource(const Resource& r): name(r.name)
{
	cout << "copy constructing " << name << endl;

}

Resource& Resource::operator=(const Resource& r)
{
	//remember if the class was big/'real' you would have to clean up before assigning new values
	name = r.getName();
	cout << "copy assignment " << name << endl;
	return *this;
}


Resource::~Resource(void)
{
	cout << "destructing " << name << endl;

}
