#pragma once
template <class T>
class Accum
{
private:
	T total;

public:
	Accum(T start): total(start) {};
	T operator+=(const T& t) {return total = total + t;}
	T getTotal() {return total;}
};

template <>
class Accum<Person>
{
private:
	int total;

public:
	Accum(int start): total(start) {};
	int operator+=(const Person& p) {return total = total + p.getNum();}
	int getTotal() {return total;}
};