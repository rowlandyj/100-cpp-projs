// classTemplateDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Acum.h"
#include "Person.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Accum<int> integers(0);
	integers += 3;
	integers += 7;
	cout << integers.getTotal() << endl;

	Accum<string> strings("");
	strings += "Hello ";
	strings += "World!";
	cout << strings.getTotal() << endl;

	//Person start("", "", 0)
	//Accum<Person> people(start) --these two lines not needed anymore
	Accum<Person> people(0);
	Person p1("R1", "J1", 123);
	Person p2("R2", "J2", 456);
	//the next two lines will cause an error unless you write a specialization (see Acum.h)
	//you could also work around the error by overload or use something like Accum<int>
	people += p1;
	people += p2;
	cout << people.getTotal().getNum() << endl;

	return 0;
}

