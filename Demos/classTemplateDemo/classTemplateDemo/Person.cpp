#include "StdAfx.h"
#include "Person.h"
#include <string>

using std::string;


Person::Person(string first, string last, int rand):
		firstName(first), lastName(last), randNum(rand)
{
}


Person::~Person(void)
{
}

string Person::getName() 
{
	return firstName + " " + lastName;
}
