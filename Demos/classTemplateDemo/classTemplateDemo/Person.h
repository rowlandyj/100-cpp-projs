#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int randNum;

public:
	Person(string first, string last, int rand);
	~Person(void);
	string getName();
	int getNum() {return randNum;}
	void setNum(int i) {randNum = i;}
};

