// stdAlgoDemo.cpp : Defines the entry point for the console application.
// using the standard library algorithms instead of your own generally makes your code more readable, flexible, and possibly faster

#include "stdafx.h"
#include <algorithm>
#include <iterator>
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

//If we for some reason decide to change vectors to lists later our code would break when we run into [i]
//Using the built in algorithms would be fine though

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> v;
	//standard for loop
	for (int i = 0; i < 5; i++)
	{
		v.push_back(i);
	}

	//have i++ in body of for loop instead of condition
	for (int i = 0; i < 5; )
	{
		v.push_back(i++);
	}
	 
	//Populating vector with std::generate
	//generate n elements,5, and insert them in vector v in the back
	int i = 0;
	std::generate_n(std::back_inserter(v), 5, 
		[&]() { return i++; }
	);
	
	//to count number of 3's in vector we can use typical for loop
	int count1 = 0;
	for (unsigned int i = 0; i < v.size(); i++)
	{
		//if there are two 3's in a row you would not remove one of the 3's
		if (v[i] == 3)
		{
			count1++;
		}
	}

	//For loop using iterators
	//a weak pt of using iterators is that they can get invalid (if you add or erase from a collection)
	int count2 = 0;
	for (auto itr = begin(v); itr != end(v); itr++)
	{
		if (*itr == 3)
		{
			count2++;
		}
	}

	//Or use std::count
	int count3 = std::count(begin(v), end(v), 3);

	//Let's remove the 3
	auto v2 = v;
	for (unsigned int i = 0; i < v2.size(); i++)
	{
		if (v2[i] == 3)
		{
			v2.erase(v2.begin() + 1);
		}
	}

	auto v3 = v;
	for (auto itr = begin(v3); itr != end(v3); itr++)
	{
		if (*itr == 3)
		{
			//this will break the code so we will just have some code to make the program run without breaking
			//v3.erase(itr);
			v3[i] = -1;
		}
	}

	auto v4 = v;
	//remove_if returns an iterator that points to the new end so we can call erase
	auto endv4 = std::remove_if(begin(v4), end(v4), [](int e) {return e == 3;});
	v4.erase(endv4, end(v4));
	//note you usually write the above two lines as 1, v4.erase(std::remove ..., end(v4));

	return 0;
}

