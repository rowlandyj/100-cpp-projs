#pragma once
#include <string>

using std::string;

class Resource
{
private:
	string name;
public:
	Resource(string n);
	Resource(const Resource& r);
	Resource& operator=(const Resource& r);
	~Resource(void);
	bool Resource::operator<(const Resource& r) {return name < r.name;}
	bool Resource::operator>(const Resource& r) {return name > r.name;}
	bool Resource::operator==(const Resource& r) {return name == r.name;}
	string getName() const {return name;}
};

