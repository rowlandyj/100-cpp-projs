// styleGuideDemo.cpp : Defines the entry point for the console application.
// some style/code writing habits to use to make code easier to read such as default params, nullptr, warnings, etc
// try to treat warnings as errors.  you can raise senstivity of errors to 4, above that, all, is too high

#include "stdafx.h"
#include "Holder.h"
#include "Resource.h"
#include <memory>

//set up with a macro don't do this since they can be redefined
#define LIMIT 3

static const int Limit = 3;

//you don't have to give numerical values here but you can if you want
enum ComputeType
{
	//if you write Simple = 4, then Complex = 5 and Insane = 6
	Simple,
	Complex,
	Insane
};

int _tmain(int argc, _TCHAR* argv[])
{
	Holder h1;
	Holder h2("two");
	Holder h3("three", 3);
	h1.increase();
	h2.increase(2);

	int i = 2;
	//using nullptr gives the added advantage of typesafe
	int *pi = nullptr;
	if (!*pi)
	{
		pi = &i;
	}
	if (pi)
	{
		//this weird line of code compiles since it equals 0 which compiler takes as 0
		pi = (2/2) - 1;
		pi = 0;
		pi = nullptr;
	}
	i = h1.someFunc(2);
	Resource r("a resource");
	i = h1.someFunc(&r);
	//we will get someFunc return of an int of 0, not overload with pointer = null;
	i = h1.someFunc(NULL);
	//with null ptr we will get the overload
	i = h1.someFunc(nullptr);

	Resource* pr = NULL;
	//this one will use the pointer overload since it knows what pr is
	i = h1.someFunc(pr);

	i = 3;
	if (i < 3)
	{
		i++;
	} else if(i < LIMIT) //no datatip on LIMIT
	{
		i++;
	} else if (i < Limit)
	{
		i++;
	}

/*don't do this
//#define LIMIT 4;
	if (i < LIMIT)
	{
		//this code will run since macro LIMIT has been redefined
		i++;
	}
*/
	//having it from the class gives the advantage of not having to worry about collisions since name held within class
	if (i < Holder::Limit)
	{
		i++;
	}

	//if you want several different constants and hold them together and use them, enum would be good to use
	ComputeType ct = Simple;
	//In visual C++, only, you can write ComputeType::Insane but you get an warning.  Usually you just write Insane.
	//Because of this you must have unique names across all enums in the namespace you are using
	if (ct != Insane)
	{
		i = 0;
	}
	if (ct >= Complex)
	{
		i++;
	}

	

	return 0;
}

