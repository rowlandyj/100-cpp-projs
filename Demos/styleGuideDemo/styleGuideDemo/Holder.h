#pragma once
#include <string>
class Resource;

using std::string;

class Holder
{
private:
	string name;
	int num;
public:
	static const int Limit = 7;
	Holder(string s = "", int n=0): name(s), num(n) {};
	~Holder(void);
	void increase(int n=1);
	int someFunc(int n) const {return n;}
//there will be a warning of an unreferenced variable, r (it's never used).  NOTE: this is viewable when warning level is 4.
//using pragma will allow the warning to be suppressed, below is the c++ idiom to use #pragma properly
#pragma warning(push) //save/push it to the stack
#pragma warning(disable : 4100) //ignore the C
	//instead of using pragma you can also take off the name, someFunc(Resource*)
	int someFunc(Resource* r) const {return -99;}
#pragma warning(pop) //pop it off from the stack

};

