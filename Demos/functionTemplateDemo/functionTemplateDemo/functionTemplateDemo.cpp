// functionTemplateDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Person.h"

using namespace std;

template <class T>
T max(T& t1, T& t2)
{
	return t1 < t2? t2: t1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "The max of 33 and 44 is " << max(33, 44) << endl;
	
	//if you wanted to do something like (33, 2.0) and wanted the double
	//return you can write max<double>(33, 2.0);

	string s1 = "Hello";
	string s2 = "World";
	cout << "The max of Hello and World is " << max(s1, s2) << endl;

	Person p1("R1", "J1", 123);
	Person p2("R2", "J2", 456);
	cout << "The max of " << p1.getName() << " and " << p2.getName() 
		<< " is " << max(p1, p2).getName() << endl;

	/*
		If you used a class that does not have an overload < 
		you will get errors.  The bad thing about the errors
		is that the burden will be on the user using the Template
		to know what operators are being used.  
	*/

	return 0;
}

