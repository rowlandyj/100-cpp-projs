#include "StdAfx.h"
#include "Person.h"
#include <string>


Person::Person(string first, string last, int rand): 
	firstName(first), lastName(last), randNum(rand)
{
}


Person::~Person(void)
{
}

std::string Person::getName()
{
	return firstName + " " + lastName;
}

bool Person::operator<(Person& p)
{
	return randNum < p.randNum;
}