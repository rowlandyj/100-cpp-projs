// ifDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	int x, y;
	cout << "Enter two numbers:" << endl;
	cin >> x >> y;

	cout << x << " ";
	//() is required for if statements in c++ {} are only necessary for multi-lines
	if(x > y)
	{
		cout << "is greater than ";
	}
	else 
	{
		cout << "is not greater than ";
	}
	cout << y << endl;

	if(x + y > 10)
	{
		cout << "thanks for choosing at least one large number" << endl;
	}
	return 0;
}

