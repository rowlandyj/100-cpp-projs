// fundamentalTypesDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


int _tmain(int argc, _TCHAR* argv[])
{
	//different ways to initialize and assign values to variables
	int i1 = 1;
	std::cout << "i1= " << i1 << std::endl;
	int i2;
	i2 = 2;
	std::cout << "i2= " << i2 << std::endl;
	int i3(3);
	std::cout << "i3= " << i3 << std::endl;

	double d1 = 2.2;
	double d2 = i1;
	//value will be truncated, also note there is a warning when you build
	int i4 = d1;
	std::cout << "d1= " << d1 << std::endl;
	std::cout << "d2= " << d2 << std::endl;
	std::cout << "i4= " << i4 << std::endl;

	//note chars can use single quote
	char c1 = 'a';
	//double quotes will be a C-style string, there is an end char so an error will come up
	//char c2 = "b";
	std::cout << "c1= " << c1 << std::endl;
	// std::cout << "c2= " << c2 << std::endl;

	bool flag = false;
	std::cout << "flag= " << flag << std::endl;
	flag = i1;
	std::cout << "flag= " << flag << std::endl;
	flag = d1;
	std::cout << "flag= " << flag << std::endl;

	//unsigned holds 8-bits (number up to 255)
	unsigned char n1 = 128;
	//holds 7-bits (number up to 127)
	char n2 = 128;
	//all couts will be symbols (may not appear correctly in console so look at debugger)
	std::cout << "n1= " << n1 << std::endl;
	//assigned to most negative number (-128)
	std::cout << "n2= " << n2 << std::endl;
	n1 = 254;
	n2 = 254;
	//n1 within range so n1 will be fine
	std::cout << "n1= " << n1 << std::endl;
	//n2 out of range n2 actually assigned to -2
	std::cout << "n2= " << n2 << std::endl;
	n1 = 300;
	n2 = 300;
	//same symbols for n1 and n2, since value out of range for both
	std::cout << "n1= " << n1 << std::endl;
	std::cout << "n2= " << n2 << std::endl;

	return 0;
}

