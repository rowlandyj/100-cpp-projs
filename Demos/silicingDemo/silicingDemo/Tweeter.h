#pragma once
#include "person.h"

class Tweeter :
	public Person
{
private:
	string twitHandle;

public:
	Tweeter(string first, string last, int i, string handle);
	~Tweeter(void);
	string getName() const;

};

