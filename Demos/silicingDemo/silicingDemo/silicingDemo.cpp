// silicingDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Tweeter.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Person localP("local", "person", 123);
	//the below code won't work because of lack of twitHandle
	//Tweeter localT = localP;

	Tweeter localT2("l2", "tweeter", 456, "@LT");
	//Without the reference we would use Person::getName()
	//References are polymorphic
	Person& localP2 = localT2;
	cout << localP2.getName() << endl;

	return 0;
}

