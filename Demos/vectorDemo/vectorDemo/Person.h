#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;

public:
	Person(string first, string last, int i);
	~Person(void);
	string getName() const;
	void setFirst(string first) {firstName = first;}
	int getId() const {return id;}
	int setId(int i) {id = i;}

};

