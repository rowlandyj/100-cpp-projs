// vectorDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <vector>
#include <iostream>

using std::cout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	/*
		Vector on the stack and has stack semantics
		So when it goes away there should be no leaks
		Vectors can also change size
		When they do they will copy the objects
		This may be costly so we can use pointers instead
		Or we pre-allocate the size of the vector
	*/


	//vectors are strongly typed
	std::vector<int> v;
	//puts a new element at the end of the vector
	v.push_back(3);
	v.push_back(9);
	v.push_back(6);
	v.push_back(2);

	//using iterator.  note: v.end points 1 past the last element
	//auto gives the compiler to decide on what the type is
	for (auto i = v.begin(); i != v.end(); i++)
	{
		cout << *i << " ";
	}
	cout << endl;

	//this will reverse the order of what is printed out
	//we are using reverse iterators.  
	for (auto i = v.rbegin(); i != v.rend(); i++)
	{
		cout << *i << " ";
	}
	cout << endl;

	/*
		In the two for loops above we use auto.
		If we used rbegin and end with auto we will get an error:
			for (auto i = v.rbegin(); i != v.end(); i++) **will not work
		because reverse end and end are different types
	*/

	int j = v[2]; //gives us the 3rd element of the vector, 6.
	cout << j << endl;

	//now lets use vectors with user made types
	std::vector<Person> vp;
	Person p1("r1", "j1", 123);
	Person p2("r2", "j2", 456);

	//we passing p1 by value which will use copy constructor of p1 to temp obj
	vp.push_back(p1);
	//destructor of p1 will be called b/c we need to reallocate to a bigger vector
	//p1 is copied over to the bigger vector and the old p1 is cleared away
	vp.push_back(p2);

	for (auto i = vp.begin(); i != vp.end(); i++)
	{
		cout << i -> getName() << " " ;
	}
	cout << endl;

	return 0;
}

