#include "StdAfx.h"
#include "Resource.h"
#include <iostream>

using std::string;
using std::cout;
using std::endl;


Resource::Resource(string n): name(n)
{
	cout << "Constructing " << name << endl;
}


Resource::~Resource(void)
{
	cout << "Destructing " << name << endl;

}
