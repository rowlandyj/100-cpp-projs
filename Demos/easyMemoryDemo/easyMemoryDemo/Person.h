#pragma once
#include <string>
#include "Resource.h"
#include <memory>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;

	/*
		shared_ptr is one of 3 smart pointers:
		shared_ptr: reference counted
		weak_ptr: lets you peek at a shared_ptr reference not counted
		unique_ptr: noncopyable (can use std::move to move things in and out)
				***dont use auto_ptr there are flaws with it***
	*/

	//with the shared_ptr we dont need the copy constructor or overload=
	//shared_ptrs also initialize themselves to empty
	std::shared_ptr<Resource> pResource;
	
public:
	Person(string first, string last, int i);
	void addRes();
	string getName() const;
	void setFirstName(string first) {firstName = first;}
	int getId() const {return id;}
	void setId(int i) {id = i;}
	//remember shared_ptrs are still ptrs; thus, we can treat them as such
	string getResName() const {return pResource->getName();}
};

