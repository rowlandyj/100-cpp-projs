// easyMemoryDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Person Row("Row", "Jiao", 123);
	Row.addRes();
	Row.setFirstName("Row2");
	Row.addRes();
	Person Row2 = Row;
	Row = Row2;
	string s1 = Row.getResName();

	return 0;
}

