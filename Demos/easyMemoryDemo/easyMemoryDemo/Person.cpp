#include "StdAfx.h"
#include "Person.h"

using std::string;

//pResource will now automatically initialize to empty
Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i)
{
}

void Person::addRes()
{
	//can't delete a pointer instead we use reset()
	//decrements reference count
	pResource.reset();
	pResource = std::make_shared<Resource>("Resource for " + getName() );
}

string Person::getName() const
{
	return firstName + " " + lastName;
}