// manualMemoryDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Resource.h"
#include <string>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	{
		Resource localRes("local");
		string localString = localRes.getName();
	}

	//Resource created on the free store (heap), note it is a pointer
	Resource* pResource = new Resource("new");
	string string1 = pResource -> getName();
	/*
		This will create a memory leak:
		int j = 3;
		if (j == 3)
		{
			return j;
		}
		Because we never call delete on pResource since it will exit before
		The destructor will never run
	*/
	Resource* p2 = pResource;
	string string2 = p2 -> getName();

	//delete will call the destructor
	delete pResource;

	//code will break since pResource has been deleted already
	//string string3 = pResource -> getName();
	

	pResource = nullptr;
	//deleting a nullptr is fine
	delete pResource;
	
	//p2, however, is pointing to memory that is already deleted
	//delete p2; you can't delete something thats already deleted

	return 0;
}

