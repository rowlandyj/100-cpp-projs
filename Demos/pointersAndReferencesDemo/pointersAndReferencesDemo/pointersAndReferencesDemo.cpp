// pointersAndReferencesDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <iostream>
#include <string>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	//pointers can be written either:
	//int* pA = &A;
	//int *pA = &A;
	
	//make an int value at 3
	int a = 3;
	cout << "a is " << a << endl;
	//make a pointer to integer using address of operator &
	int* pA = &a;
	//going through pointer to integer to change its value
	*pA = 4;
	cout << "a is " << a << endl;
	int b = 100;
	//now pointer to b
	pA = &b;
	//increments value of b by 1, a no change
	//note how you need ()
	(*pA)++;
	cout << "a " << a << ", *pa " << *pA << endl;

	Person p1("R1", "J1", 123);
	Person* pP1 = &p1;
	(*pP1).setId(456);
	cout << "p1: " << p1.getName() << " " << p1.getId() << endl;
	//below shows 2 different notations to work through the pointer (*) and ->
	cout << "p1: " << (*pP1).getName() << " " << pP1 -> getId() << endl;

	//integer reference rA.  This must be assigned when declared unlike pointers
	int& rA = a;
	//below will actually change a to 5 since rA is an alias for a
	rA = 5;
	cout << "a is " << a << endl;

	Person& rP1 = p1;
	rP1.setId(789);
	cout << "rP1: " << rP1.getName() << " " << rP1.getId() << endl;

	/*
		The commented code below will cause an error (app will "blow up")
		badPointer is not set to the address of anything
		then we try to go through badPointer and set it to 3

		We can make this slightly better by initializing baPointer to nullptr
		with nullptr can run an if-statement to avoid breaking the app
	*/

	//int* badPointer;
	//*badPointer = 3;
	//cout << badPointer << endl;
	int* badPointer = nullptr;
	if (badPointer) 
	{
		*badPointer = 3;
		cout << badPointer << endl;
	}


	/*
		There's no such thing as a null reference
		References must be initialized, pointing to something real
		Hence the code below will always break
	*/

	//int& badRef;
	//badRef = 3;
	//cout << badRef;

	return 0;
}

