#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;

public:
	Person(string first, string last, int i);
	~Person();
	string getName();
	int getId() {return id;}
	void setId(int i) {id = i;}
};