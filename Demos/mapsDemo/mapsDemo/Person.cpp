#include "StdAfx.h"
#include "Person.h"
#include <string>

using std::string;

Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i)
{
}

Person::Person():
	firstName(""), lastName(""), id(0)
{
}


Person::~Person(void)
{
}

string Person::getName() const
{
	return firstName + " " + lastName;
}