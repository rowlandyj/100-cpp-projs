// mapsDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <iostream>
#include <map>

using std::cout;
using std::endl;


int _tmain(int argc, _TCHAR* argv[])
{
	//mapping integers to Person objects
	std::map<int, Person> people;
	Person p1("r1", "j1", 10);
	Person p2("r2", "j2", 12);
	
	//the sqr bracket way to add things to map
	//note: you will need a default constructor
	people[p1.getId()] = p1;
	people[p2.getId()] = p2;

	//another way to add to the map
	Person p3("r3", "j3", 11);
	//std::pair object, a template that takes 2 types
	std::pair<int, Person> pPair(p3.getId(), p3);
	//using .insert(std::pair) to populate map
	people.insert(pPair);

	//iterate through map like through a vector
	//see how the map will keep it sorted internally
	for (auto i = people.begin(); i != people.end(); i++)
	{
		//map iterator points to a pair, first and second
		//first is an int and second is a Person obj in this demo
		cout << i -> first << " " << i -> second.getName() << ". ";
	}
	cout << endl;
	
	//To look things up in a map use .find(key)
	//find returns an iterator. so you will need to dereference it
	auto found = people.find(10); //there will be an error if you use an invalid key
	cout << found -> first << " " << found -> second.getName() << endl;

	//Another way to look it up is use []
	string person1 = people[11].getName(); //returns null if [key] doesn't exist
	cout << "person with id 11 is " << person1 << endl;

	return 0;
}

