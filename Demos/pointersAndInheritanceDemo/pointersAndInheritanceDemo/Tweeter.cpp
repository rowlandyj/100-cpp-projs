#include "StdAfx.h"
#include "Tweeter.h"


Tweeter::Tweeter(string first, string last, int i, string handle):
	Person(first, last, i), twitHandle(handle)
{
}


Tweeter::~Tweeter(void)
{
}

string Tweeter::getName() const
{
	return Person::getName() + " " + twitHandle;
}
