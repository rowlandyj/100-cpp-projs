// pointersAndInheritanceDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Tweeter.h"
#include <iostream>
#include <memory>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	//creating a Person and Tweeter on free store
	Person* Row = new Person("Row", "Jiao", 123);
	Tweeter* Rowt = new Tweeter("R", "J", 456, "@RJ");

	Person* pRowt = Rowt;

	cout << Row -> getName() << endl;
	cout << Rowt -> getName() << endl;
	//we will get the Tweeter::getName() since Person::getName() is virtual
	cout << pRowt -> getName() << endl;

	//since we use virtual destructor on Person, the Tweeter one will run
	delete pRowt;

	//see how there are 2 <Person> which seems kind of redundant.  We can use auto.
	//shared_ptr<Person> spRow = make_shared<Person>("R2", "J2", 789, "@RJ2");
	auto spRow = make_shared<Tweeter>("R2", "J2", 789, "@RJ2");
	cout << spRow -> getName() << endl;

	//our shared_ptrs are being appropriately polymorphic
	shared_ptr<Person> spRow2 = make_shared<Tweeter>("R3", "J3", 789, "@RJ3");
	cout << spRow2 -> getName() << endl;


	return 0;
}

