#include "StdAfx.h"
#include "Person.h"
#include <iostream>

using namespace std;


Person::Person(string first, string last, int rand) : 
			firstName(first), lastName(last), randNum(rand)
{
	cout << "constructing " << getName() << endl; 
}


Person::~Person(void)
{
	cout << "destructing " << getName() << endl; 

}

string Person::getName()
{
	return firstName + " " + lastName;
}

