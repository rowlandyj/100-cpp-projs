#pragma once
#include "person.h"
#include <string>

using std::string;

class Tweeter :
	public Person
{
private:
	string twitHandle;

public:
	Tweeter(string first, string last, int rand, string handle);
	~Tweeter(void);
};

