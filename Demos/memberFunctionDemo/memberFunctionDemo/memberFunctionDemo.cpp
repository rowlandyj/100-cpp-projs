// memberFunctionDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Tweeter.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("R1", "J1", 123);
	{
		Tweeter t1("R2", "J2", 456, "@RJ");
	}
	cout << "after inner most bracket" << endl;
	cout << "p1: " << p1.getName() << " " << p1.getNumber() << endl;
	p1.setNumber(789);
	cout << "p1: " << p1.getName() << " " << p1.getNumber() << endl;
	return 0;
}

