#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int randNum;

public:
	Person(string first, string last, int rand);
	~Person(void);
	string getName();
	//below are 2 inline functions which are faster than regular functions  
	int getNumber() {return randNum;}
	void setNumber(int num) {randNum = num;}
};

