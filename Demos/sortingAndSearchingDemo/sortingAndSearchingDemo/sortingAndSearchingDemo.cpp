// sortingAndSearchingDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <iostream>
#include <map>
#include <vector>
//needed for sorting and for_each
#include <algorithm>

using std::cout;
using std::endl;

//instead of these 2 small fns we can use lambdas
//lambdas will be covered in another demo
void print(int i)
{
	cout << i << " ";
}

bool odd(int i)
{
	return i % 2 != 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("r1", "j1", 10);
	Person p2("r2", "j2", 11);
	Person p3("r3", "j3", 12);

	//here's a neat way to initialize a vector using .insert
	int a[] = {3, 6, 9, 2};
	std::vector<int> v;
	v.insert(v.end(), a, a+4); 
	//review of making a vector and map
	std::vector<Person> vPeople;
	vPeople.push_back(p1);
	vPeople.push_back(p2); 
	vPeople.push_back(p3);
	std::map<int, Person> mPeople;
	mPeople[p1.getId()] = p1;
	mPeople[p2.getId()] = p2;
	mPeople[p3.getId()] = p3;

	//for each makes every element do in the last parameter
	//in this case we make each element print(defined above)
	std::for_each(v.begin(), v.end(), print);
	cout << endl;

	//using find_if we can get every element that meets a condition (last parameter)
	//in this case we are finding every odd integer.  find_if returns an iterator, a ptr
	auto o = find_if(v.begin(), v.end(), odd);
	//if there wasn't an odd integer we would not enter the while loop
	while (o != v.end())
	{
		cout << *o << " ";
		o = find_if(++o, v.end(), odd);
	}
	cout << endl;

	//Finally to sort we just call sort(iterator, iterator);
	std::sort(v.begin(), v.end());
	for_each(v.begin(), v.end(), print);
	cout << endl;

	return 0;
}

