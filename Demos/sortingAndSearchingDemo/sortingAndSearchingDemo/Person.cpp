#include "StdAfx.h"
#include "Person.h"

using std::string;

Person::Person(void):
	firstName(""), lastName(""), id(0)
{
}

Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i)
{
}

Person::~Person(void)
{
}

string Person::getName() const
{
	return firstName + " " + lastName;
}
