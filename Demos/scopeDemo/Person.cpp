#include "stdafx.h"
#include "Person.h"
#include <iostream>

Person::Person(std::string first, std::string last, int rand) : firstName(first), lastName(last), randNum(rand)
{
	std::cout << "constructing " << firstName << " " << lastName << std::endl;
}

Person::~Person()
{
	std::cout << "destructing " << firstName << " " << lastName << std::endl;
}
