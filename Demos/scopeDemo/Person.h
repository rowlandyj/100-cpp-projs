#include <string>

class Person
{
private:
	std::string firstName;
	std::string lastName;
	int randNum;

public:
	//constructor
	Person(std::string first, std::string last, int rand);
	//destructor
	~Person();
};