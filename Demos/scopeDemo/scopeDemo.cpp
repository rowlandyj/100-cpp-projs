// scopeDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"

/*
	Resource Acquisition is Initialization: RAII
	-Acquire resources in constructor
	-Release them in destructor
*/
int _tmain(int argc, _TCHAR* argv[])
{
	//Scope usually ends when it ends a closing brace bracket
	Person p1("Row", "Jiao", 123);
	{
		Person p2("R", "J", 456);
	}
	return 0;
}

