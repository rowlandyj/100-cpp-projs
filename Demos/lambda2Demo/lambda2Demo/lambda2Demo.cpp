// lambda2Demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;


int _tmain(int argc, _TCHAR* argv[])
{
	int x = 0;
	int y = 0;
	std::vector<int> v;
	//assigning a lambda to a var, x and y will be captured by ref
	//print will use the val of x and y at the time of capture so it must be ref
	//ie if we use [=] all out print() calls below will be the same, x:0 y:0
	auto print = [&](){cout << "x: " << x << " y: " <<  y << endl;};

	for(int i = 0; i < 5; i++) { v.push_back(i);}

	//mutable is needed or else x and y will be created as const 
	//everything captured by value
	std::for_each(begin(v), end(v), [=] (int e) mutable {
		x += e;
		y += e;
	});
	print();

	//everything by reference except x is by value
	std::for_each(begin(v), end(v), [&, x] (int e) mutable {
		x += e;
		y += e;
	});
	//no change in x
	print();
	x = y = 0;

	//everything by value except x is by reference
	std::for_each(begin(v), end(v), [=, &x] (int e) mutable {
		x += e;
		y += e;
	});
	//no change in y
	print();
	x = y = 0;

	//everything by reference
	std::for_each(begin(v), end(v), [&] (int e) mutable {
		x += e;
		y += e;
	});
	//changes in both x and y
	print();
	x = y = 0;

	return 0;
	
	//note we are passing in a size for doubleV since we know it should be same size as v
	std::vector<int> doubleV(v.size());
	std::transform(begin(v), end(v), begin(doubleV), 
		[](int e) {return e * 2;}
	);

	//you can all fns in lambdas, so try to keep em fairly short for readability
}

