#pragma once
#include "Person.h"
#include <string>

class Tweeter :
	public Person
{
private:
	std::string twitHandle;

public:
	Tweeter(std::string first, std::string last, int rand, std::string handle);
	~Tweeter(void);
};

