#include <string>

class Person
{
private:
	std::string firstName;
	std::string lastName;
	int randNum;

public:
	Person(std::string first, std::string last, int rand);
	~Person();
};