// inheritanceDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//No #include "Person.h", double inclusion of base you will get a compilation error (Tweeter.h has #include Person.h)
//See enumAndNameSpaceDemo to see how include guards are used
#include "Tweeter.h"
#include <iostream>


int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("R1", "J1", 123);
	{
		//the Person part of the Tweeter constructor is called as well
		Tweeter t1("R2", "J2", 456, "@RJ");
	}

	std::cout << "after innermost block" << std::endl; 
	return 0;
}

