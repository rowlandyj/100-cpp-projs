#include "StdAfx.h"
#include "Tweeter.h"
#include <iostream>


Tweeter::Tweeter(std::string first, std::string last, 
	int rand, std::string handle): Person(first, last, rand), twitHandle(handle)
{
	std::cout << "constructing tweeter " << twitHandle << std::endl;

}


Tweeter::~Tweeter(void)
{
	std::cout << "destructing tweeter " << twitHandle << std::endl;

}
