#pragma once
#include <string>

using std::string;

class Resource
{
private:
	string name;

public:
	Resource(string n);
	Resource(const Resource& r);
	Resource& operator=(const Resource& r);
	//&& is a r-value reference, take away const because we will pretty much strip the old object
	//r values are ephemeral and will go away soon, once it's used it will be thrown away
	Resource(Resource&& r);
	Resource& operator=(Resource&& r);
	~Resource(void);
	bool Resource::operator>(const Resource& r) {return name > r.getName();}
	bool Resource::operator<(const Resource& r) {return name < r.getName();}
	bool Resource::operator==(const Resource& r) {return name == r.getName();}	
	string getName() const {return name;}
};

