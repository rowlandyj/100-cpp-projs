#include "StdAfx.h"
#include "Resource.h"
#include <iostream>

using std::cout;
using std::endl;


Resource::Resource(string n): name(n)
{
	//cout << "constructing " << name << endl;
}

Resource::Resource(const Resource& r): name(r.name)
{
	//cout << "copy constructing " << name << endl;

}

Resource& Resource::operator=(const Resource& r)
{
	name = r.getName();
	//cout << "copy assignment " << name << endl;
	return *this;
}

//other r has no name
Resource::Resource(Resource&& r): name(std::move(r.name))
{
}

Resource& Resource::operator=(Resource&& r)
{
	if (this != &r)
	{
	name = std::move(r.name);
	r.name.clear();
	//cout << "copy assignment " << name << endl;
	}
	return *this;
}

Resource::~Resource(void)
{
	//cout << "destructing " << name << endl;

}
