// listAndVector2Demo.cpp : Defines the entry point for the console application.
// See how vector will perform better with objects with move constructor and operator
// also see r-value referencer

#include "stdafx.h"
#include "Resource.h"
#include <iostream>
#include <algorithm>
#include <list>
#include <vector>
#include <iterator>
#include <random>

using std::cout;
using std::endl;
using std::string;

#undef UNICODE
#include <Windows.h>


template <typename T>
void exercise(int size, string commonname)
{
	T v;
	//res1 will be whatever value-type T is
	T::value_type res1("starting entry");
	v.push_back(res1);
	for (int i = 0; i < size; i++)
	{
		int r = (int) rand();
		string s;
		//gets last two digits of our random number
		int ss = r % 100;
		s.reserve(ss);
		//generate ss copies of whatever ascii char that is 'a' + ss/2
		std::generate_n(std::back_inserter(s), ss, [ss]() {return (char) ('a' + ss/2);});
		T::value_type res(s);
		bool inserted = false;
		for (auto it = begin(v); it != end(v); it++)
		{
			if (*it > res)
			{
				v.insert(it, res);
				inserted = true;
				break;
			}
		}
		if (!inserted)
		{
			v.push_back(res);
		}
	}

	//max_element(start range, end range)
	auto it = max_element(begin(v), end(v));
	T::value_type maxV = *it;
	//to be able to use this for Resources we have to call .getName()
	cout << "max elem in " << commonname << ": " << maxV.getName() << endl;

	it = min_element(begin(v), end(v));
	T::value_type minV = *it;
	cout << "min elem in " << commonname << ": " << minV.getName() << endl;

	bool sorted = is_sorted(begin(v), end(v));
	cout << "the " << commonname << "is ";
	if (!sorted)
	{
		cout << "not ";
	}
	cout << "sorted." << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	const int size = 1000;
	cout << "Running vector and list of " << size << " elements" << endl;

	_int64 ctr1 = 0, ctr2 = 0, freq = 0;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);

	exercise<std::vector<Resource>>(size, "vector");
	QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);

	double time = ((double) (ctr2 - ctr1)) * 1000.0/(double) freq;
	cout << "vector time (ms): " << time << endl;

		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);

	exercise<std::vector<Resource>>(size, "list");
	QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);

	time = ((double) (ctr2 - ctr1)) * 1000.0/(double) freq;
	cout << "vector time (ms): " << time << endl;

	/*
		For strings performance isn't that big of a difference between the two. For Resources, however,
		without move constructors and move assignment operator we see that vectors perform much worse than lists
		with the move constructor and assignment vector can use that to be move efficient.
		in this demo list will perform better than vector regardless but the difference between the two will be smaller
	*/

	return 0;
}

