#include <string>

class Person
{
private:
	std::string firstName;
	std::string lastName;
	int randNum;

public:
	//if you don't write a consturctor a default constructor will be used
	Person(std::string first,
		std::string last,
		int rand);

};