// classesAndObjectsDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//values for most types in cpp are not defaulted or initialized (eg: int will not be initialized to something)
	//objects can have values initialized (eg: String initialized to "") via constructor
	Person p1("Rowland", "Jiao", 123);
	Person p2("R", "J", 456);
	return 0;
}

