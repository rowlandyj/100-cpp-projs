#include "stdafx.h"
#include "Person.h"

/*
	Do Not Do This In CPP
Person::Person(std::string first, std::string last, int rand)
{
	firstName = first;
	lastName = last;
	randNum = rand;
}

	all member variables are given memory and values are defaulted if applicable
	then once the code reaches the open bracket above all member variable values are changed
	this can be a huge waste of time in this basic demo its not a big deal but its awful habit
	instead use initializer syntax
	
*/


//have to give full name (include class name).  className::functionName
Person::Person(std::string first, std::string last, int rand) : firstName(first), lastName(last), randNum(rand)
{
}