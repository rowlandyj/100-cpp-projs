// freeFunctionDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Utility.h"

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	int x;
	cout << "Enter a number" << endl;
	cin >> x;

	//again reminder the {} are not necessary since theres only 1 action
	if (isPrime(x))
	{
		cout << x << " is a prime" << endl;
	}
	else
	{
		cout << x << " is not a prime" << endl;
	}

	//just to prove you don't need {}
	if (is2MorePrime(x))
		cout << x << "+2 is a prime" << endl;
	else
		cout << x << "+2 is not a prime" << endl;

	return 0;
}

