bool isPrime(int x);

//note that x is being based by value
//if you want to pass by reference use int& instead of int
bool is2MorePrime(int x);

//example of a dangling reference
//int& badFunction()