#include "stdafx.h"
#include <iostream>
using namespace std;

#include "Utility.h"

bool isPrime(int x)
{
	bool prime = true;
	for (int i = 2; i <= x / i; i++)
	{
		int factor = x / i;
		if (factor * i == x)
		{
			cout << "factor found: " << factor << endl;
			prime = false;
			break;
		}

	}
	return prime;
}

//note that x is being based by value
//if you want to pass by reference use int& instead of int
bool is2MorePrime(int x)
{
	/* We can also write this as
	x = x + 2;
	return isPrime(x);
	*/
	return isPrime(x + 2);
}

/*Example of dangling reference

	Below the function creates a local var and returns a reference to a
	but then a goes out of scope, and code that tries to call that function
	and use the value from the function may only work sometimes
	if you compile this there will be a compiler warning

int& badFunction()
{
	int a = 3;
	return a;
}

*/