// lamdaDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <algorithm>
//needed for back_inserter
#include <iterator>
#include <iostream>

using std::cout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> v;
	for (int i = 0; i < 10; ++i)
	{
		v.push_back(i);
	}

	cout << "-----lambda used to print elements----" << endl;
	//lambdas are set off by [](params)
	std::for_each(v.begin(), 
				v.end(), 
				[](int n) 
				{
					cout << n << " ";
				}
	);
	cout << endl;

	cout << "-----lambda that is multi line----" << endl;
	//lambdas can also have multiple lines/actions
	//they usually 1 line long
	std::for_each(v.begin(), v.end(),
		[](int n)
		{
			if (n % 2)
			{
				cout << "even" << endl;
			} else
			{
				cout << "odd" << endl;
			}
		});

	/*
	lambdas can also return a value
	the return type can be written as:
		[](int n) -> doubl {...}
	usually the compiler can figure it out
	*/

	cout << "-----lambda returning a value----" << endl;
	//lambda returning a value
	std::vector<int> v2;
	std::transform(v.begin(), v.end(), std::back_inserter(v2), [](int n) {return n * n * n;});
	std::for_each(v2.begin(), v2.end(), [](int n) {cout << n << " ";});
	cout << endl;

	cout << "-----lambda specifying return type----" << endl;
	//lambda specifying return type
	std::vector<double> dv;
	std::transform(v.begin(), v.end(), std::back_inserter(dv), 
		[](int n) -> double {
			if (n % 2 == 0) {
				return n * n * n;
			} else {
				return n / 2.0;
			}
		});
	std::for_each(dv.begin(), dv.end(), [](double n) {cout << n << " ";});
	cout << endl;

	cout << "-----lambda explicit capture----" << endl;
	//so far we have been using an empty capture for lambda, []
	//now let's use capture explicitly [x,y]
	int x = 3;
	int y = 7;
	//print out elements between 3 and 7, inclusive
	std::for_each(v.begin(), v.end(), [x,y](int n) {
		if (n >= x && n <= y)
		{
			cout << n << " " ;
		}
	});
	cout << endl;

	cout << "-----lambda implicit capture by value----" << endl;
	//now let's use capture whole stack [=] by value
	//will take in everything
	int a = 1;
	int b = 9;
	//prints out 1, 9, and everything between 3 and 7
	std::for_each(v.begin(), v.end(), [=](int n) {
		if ((n >= x && n <= y) || (n == a || n == b))
		{
			cout << n << " " ;
		}
	});
	cout << endl;

	cout << "-----lambda value capture mutable----" << endl;
	//The terms that we have captured, x,y,a,b, are const in lambda
	//To make it so we can change em we add mutable
	std::for_each(v.begin(), v.end(), [=](int& r) mutable {
		const int old = r;
		r *= 2;
		//the two lines below are only able to run b/c we used mutable
		x = y;
		y = old;
	});

	cout << "-----changed vector----" << endl;
	//Thus far, we have not changed vector v in any way
	//B/c we used int& we have made it so the vector values will change
	std::for_each(v.begin(), v.end(), [=](int n) {
		cout << n << " ";
	});
	cout << endl;

	cout << "-----x and y unchanged because value capture----" << endl;	
	//Please note that since we passed by value, x and y are unchanged
	cout << x << " " << y << endl;

	cout << "-----lambda capture by reference----" << endl;
	//lets clear the vector and reinitialize it to what we had at the start 0-9
	v.clear();
	for (int i = 0; i < 10; i++) {v.push_back(i);}
	//Now to capture by reference we use [&x, &y]
	//x = 3, y = 7
	std::for_each(v.begin(), v.end(), [&x, &y](int& n){
		const int old = n;
		n *= 2;
		//the two lines below are only able to run b/c we used mutable
		x = y;
		y = old;
	});

	cout << "-----changed vector----" << endl;
	std::for_each(v.begin(), v.end(), [=](int n) {
		cout << n << " ";
	});
	cout << endl;
	cout << "-----x and y changed because ref capture----" << endl;	
	cout << x << " " << y << endl;

	cout << "-----whole stack ref capture----" << endl;
	//again clear vector and repopulate it, this time using a new method
	v.clear();
	int i = 0;
	//since we are capture full stack we dont need (param) 
	//generate_n takes a transformation fn and do it x times, 10 in this case
	std::generate_n(back_inserter(v), 10, [&] {return i++;});
	//vector of 0-9
	cout << "-----original vector again----" << endl;
	std::for_each(v.begin(), v.end(), [](int n) { cout << n << " " ;});
	cout << endl;
	cout << "-----changed vector----" << endl;
	cout << "i is " << i << endl;

	return 0;
}

