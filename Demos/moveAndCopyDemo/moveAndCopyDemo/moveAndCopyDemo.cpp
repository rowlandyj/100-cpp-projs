// moveAndCopyDemo.cpp : Defines the entry point for the console application.
// Exploring the power of using move constructors and operators more

#include "stdafx.h"
#include "Resource.h"
#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	Resource r("local");
	cout << "--------------------" << endl;

	vector<Resource> resources;
	//.push_back() takes Resource by value
	//r is still copied over, not moved because r is not a r-value (it will still sit on stack)
	resources.push_back(r);
	cout << "--------------------" << endl;

	//vector will expand, "first" is temp so it will be moved
	resources.push_back(Resource("first"));
	cout << "--------------------" << endl;

	//because we pass by value we will copy and destruct Resources when lambda is called
	//it will not use move constructor since it would be inappropriate to delete/strip the old obj in vector
	//you can instead call res by reference [](const Resource& res) to avoid copying and deleting the copies
	std::for_each(begin(resources), end(resources), 
		[](const Resource& res) {cout << res.getName() << endl;
	});
	cout << endl;
	return 0;
}

