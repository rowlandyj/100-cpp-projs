#include "StdAfx.h"
#include "Resource.h"
#include <iostream>

using std::cout;
using std::endl;


Resource::Resource(string n): name(n)
{
	cout << "constructing " << name << endl;
}

Resource::Resource(const Resource& r): name(r.name)
{
	cout << "copy constructing " << name << endl;

}

Resource& Resource::operator=(const Resource& r)
{
	name = r.getName();
	cout << "copy assign " << name << endl;

	return *this;
}

Resource::Resource(Resource&& r): name(std::move(r.name))
{
	cout << "move constructing " << name << endl;

}

Resource& Resource::operator=(Resource&& r)
{
	cout << "move assign " << name << endl;
	if (this != &r)
	{
		name = std::move(r.name);
		r.name.clear();
	}
	return *this;
}

Resource::~Resource(void)
{
	cout << "destructing " << name << endl;

}
