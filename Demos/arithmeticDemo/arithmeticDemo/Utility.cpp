#include "stdafx.h"
#include "Utility.h"

using namespace std;

bool isPrime(int x)
{
	bool prime = true;
	for(int i = 2; i <= x / i; i++)
	{
		int factor = x / i;
		if (factor * i == x)
		{
			prime = false;
			break;
		}
	}

	return prime;
}