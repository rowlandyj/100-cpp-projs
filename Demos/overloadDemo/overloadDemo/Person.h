#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int randNum;

public:
	Person(string first, string last, int rand);
	~Person(void);
	string getName();
	int getNum() {return randNum;}
	void setNum(int num) {randNum = num;}
	//2 member function overloads.  Note how the first has Person as reference
	//passing by reference is slightly faster
	bool operator<(Person& p);
	bool operator<(int i);

	/*	if there is no getNum() function for this we can do the line below
		friend bool operator<(int i, Person& p);
		note that the above line is not declaring the function we still need it below
		with this we can use p.randNum
	*/
};

//here is a free function to deal with integers compared to Person
//this should probably be dealt/written by the person writing the Person class
//hence y it is here...there's no actual rule
bool operator<(int i, Person& p);

