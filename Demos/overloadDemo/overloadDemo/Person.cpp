#include "StdAfx.h"
#include "Person.h"


Person::Person(string first, string last, int rand) : firstName(first), lastName(last), randNum(rand)
{
}


Person::~Person(void)
{
}

string Person::getName()
{
	return firstName + " " + lastName;
}

bool Person::operator<(Person& p)
{
	return randNum < p.randNum;
}

bool Person::operator<(int i)
{
	return randNum < i;
}

bool operator<(int i, Person& p)
{
	//if no getNum() fn and we added friend operator< we can use
	//return i < p.randNum;
	return i < p.getNum();
}