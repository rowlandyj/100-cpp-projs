// overloadDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <string>
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("R1", "J1", 123);
	Person p2("R2", "J2", 456);
	
	cout << "p1 is ";
	if (!(p1 < p2))
	{
		cout << "not ";
	}
	cout << "less than p2" << endl;

	cout << "p1 is ";
	if (!(p1 < 300))
	{
		cout << "not ";
	}
	cout << "less than 300" << endl;

	cout << "300 is ";
	if (!(300 < p1))
	{
		cout << "not ";
	}
	cout << "less than p1" << endl;



	return 0;
}

