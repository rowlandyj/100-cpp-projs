#pragma once
#include <string>

using std::string;

//pretend this is a big obj and require managing
class Resource
{
private:
	string name;

public:
	Resource(string n);
	Resource(const Resource& r);
	Resource& operator=(const Resource& r);
	~Resource(void);
	string getName() const {return name;}
};

