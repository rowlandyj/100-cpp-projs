#pragma once
#include "Resource.h"
#include <string>
#include <memory>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;
	//solid member var example:
	//Resource pResource;
	
	std::unique_ptr<Resource> pResource;

	//if we use shared pointers we wont need copy construct or oper
	//std::shared_ptr<Resource> pResource;
	
	//if we want it so Person cannot be copied
	//declare the copy const and operator in private
	//Person(const Person& p);
	//Person& operator=(const Person& p);



public:
	Person(string first, string last, int i);
	~Person(void);
	//for deep copy ability we will need to declare copy operator and constructor
	Person(const Person& p);
	Person& operator=(const Person& p);
	string getName() const;
	void setFirstName(string first) {firstName = first;}
	int getId() const {return id;}
	void setId(int i) {id = i;}

	void setResource(string rName);
	string getResource() const {return pResource->getName();}

};

