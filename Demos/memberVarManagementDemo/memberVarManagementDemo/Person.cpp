#include "StdAfx.h"
#include "Person.h"


Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i) //, pResource("")    <--only necessary is member var instead of ptr
{
}

Person::~Person(void)
{
}

Person::Person(const Person& p):
	firstName(p.firstName), lastName(p.lastName), id(p.id)
{
	pResource.reset(new Resource(p.getResource()));
}

Person& Person::operator=(const Person& p)
{
	firstName = p.firstName;
	lastName = p.lastName;
	id = p.id;
	//reset will auto clear the old one
	pResource.reset(new Resource(p.getResource()));

	return *this;
}

string Person::getName() const 
{
	return firstName + " " + lastName;
}

void Person::setResource(string rName) 
{
	/*  Below is code used when pResource is a member var instead of ptr:
		Resource is big so this is wasteful behavior
		the setResource constructs the Resource on local stack and copies
		it to member variable. then deletes the local Resource
	Resource newResource(rName);
	pResource = newResource;
	*/

	//we need new to get something out on free store
	//.reset() value, default of null, to something else
	pResource.reset(new Resource(rName));

	//for shared_ptr we just need to use make_shared
	//pResource = make_shared<Resource>(rName);
}
