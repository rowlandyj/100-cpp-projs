// memberVarManagementDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"

using std::string;


int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("R1", "J1", 123);
	//When using member var the setResource constructs the Resource on 
	//local stack and copies it to member var. then deletes the local Resource
	p1.setResource("p1 Resource");
	//constructs new one, then destructs old one which saves you from self assignment
	p1.setResource("p1 2nd Resource");
	
	//we have to decide whether they share the resource or we make a copy of it
	//usually we want resource to be different, a deep copy
	Person p2 = p1;
	//this just copy assigns the Resource
	p1 = p2;
	string s1 = p1.getResource();
	return 0;
}

