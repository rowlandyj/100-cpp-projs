//not all compilers can use pragma once
#pragma once //this line of code is similar to having #ifndef, #define, #endif
#include "person.h"
#include <string>
using std::string;

class Tweeter :
	public Person
{
private:
	string twitHandle;

public:
	Tweeter(string first, string last, int rand, string handle);
	~Tweeter(void);
};

