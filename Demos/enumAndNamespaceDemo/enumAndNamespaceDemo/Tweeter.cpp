#include "StdAfx.h"
#include "Tweeter.h"
#include <iostream>

using namespace std;

Tweeter::Tweeter(string first, string last, int rand, string handle): Person(first, last, rand), twitHandle(handle)
{
	cout << "constructing tweeter " << twitHandle << endl;
}


Tweeter::~Tweeter(void)
{
	cout << "destructing tweeter " << twitHandle << endl;
}
