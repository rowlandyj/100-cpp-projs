#include "stdafx.h"
#include "Person.h"
#include <iostream>

using namespace std;

Person::Person(string first, string last, int rand): firstName(first), lastName(last), randNum(rand)
{
	cout << "constructing " << firstName << " " << lastName << endl;
}

Person::~Person()
{
	cout << "destructing " << firstName << " " << lastName << endl;

}