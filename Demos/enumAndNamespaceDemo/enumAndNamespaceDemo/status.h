enum Status
{
	//Only use Internal names (ie: we will not use status.pending but Pending)
	//Names below must be unique
	Pending,
	Approved,
	Cancelled
};