// enumAndNamespaceDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Tweeter.h"
#include "status.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Person p1("R1", "J1", 123);
	{
		Tweeter t1("R2", "J2", 456, "@rj");
	}
	
	cout << "after innermost block" << endl;
	Status s = Pending;
	s = Approved;

	return 0;
}

