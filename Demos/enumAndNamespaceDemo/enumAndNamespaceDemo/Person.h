#include <string>
//allows main to ave #include Person.h and throw no errors
#ifndef _PERSON_H
#define _PERSON_H

//for headers avoid using the entire namespace of something stick with specifics like below
using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int randNum;

public:
	Person(string first, string last, int rand);
	~Person();
};
#endif