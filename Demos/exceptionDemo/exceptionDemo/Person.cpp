#include "StdAfx.h"
#include "Person.h"
#include "Noise.h"
#include <iostream> 



Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i)
{
	Noise np("person noise");

	if (id == 0)
	{
		throw std::invalid_argument("id for a person can't be 0);");
	}
}

Person::Person(): firstName(""), lastName(""), id(0)
{
}

Person::~Person(void)
{
}

string Person::getName() const
{
	return firstName + " " + lastName;
}
