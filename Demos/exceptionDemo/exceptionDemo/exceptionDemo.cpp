// exceptionDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Noise.h"
#include <iostream>
#include <vector>

using namespace std;

/*
	While Exceptions are great in catching problems they do have a cost
	If no exception is thrown then the cost is negligible
	However, if an if-statement can be used do use it, it will cost less
	Eg: never test for date validation
	Exceptions are used when there is a deep hierarchy:
		A calls b calls c calls d...
		code will look neater and run faster since there isnt a bunch of if's
	Best used for rare occurences such as disk full and network failure
*/


int _tmain(int argc, _TCHAR* argv[])
{
	//gets destructed last because it was defined before the try
	Noise n1("s1");
	//if you are going to use pointers in the try block use the smart ptrs
	//make_shared<blah> will prevent memory leaks from happening
	try
	{
		vector<int> v;
		v.push_back(1);
		//destructor will run once it runs into exception
		//destructor of Noise in Person will also run
		Noise n1("s2");

		//because of this line of code we will actually skip the out of range exception
		//due to the throw we have in constructor, std::invalid_argument
		Person badPerson("bad", "id", 0);

		//this is out of range and will throw an exception, out of range
		//we need to catch this
		int j = v.at(99);
	}

	//try block can have any # of catches, have the most specific one first
	//first catch block that can get it will be the only one that run
	catch (out_of_range&  e)
	{
		cout << "out of range exception " << e.what() << endl;
	}
	//catches must use reference or slicing will occur
	//in this example we will lose invalid_argument type
	catch (exception& e) 
	{
		cout << e.what() << endl;
	}
	return 0;
}

