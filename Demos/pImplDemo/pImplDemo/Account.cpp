#include "StdAfx.h"
#include "Account.h"
#include "AccountImpl.h"

//delegate to pImpl's constructor
Account::Account(int bal, Customer* cust):
	pImpl(new AccountImpl(bal, cust))
{
}

//this must be here (does not need to have functionality) because AccountImpl.h has been included which includes
//info whether or not impl has a destructor.  Without this the compile generated destructed which would be in the header file
//would not be properly generated
Account::~Account(void)
{
}

Account::Account(Account&& otherAcct): pImpl(std::move(otherAcct.pImpl))
{
}

Account& Account::operator=(Account&& otherAcct)
{
	pImpl = std::move(otherAcct.pImpl);
	return *this;
}

//the business logic will be delegated to the implementation's class functions
int Account::deposit(int amt)
{
	return pImpl-> deposit(amt);
}

int Account::withdraw(int amt)
{
	return pImpl-> withdraw(amt);
}