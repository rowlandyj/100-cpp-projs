#pragma once
#include <string>
#include <memory>
class Customer;
//fwd declare AcctImpl, never #include AccountImpl here
class AccountImpl;

using std::string;
class Account
{
private:
	//pImpl is the expected name, but you can call it p
	std::unique_ptr<AccountImpl> pImpl;

public:
	Account(int bal, Customer* cust);
	//destructor must be declared here
	~Account(void);
	//moving is ok, but no copying (from a business point of view copying for this example would be awful)
	//by using the unique_ptr, by default, this is uncopyable anyways.
	Account(Account&& otherAcct);
	Account& operator=(Account&& otherAcct);
	//don't implement these in the header file, will make compile time slower
	int deposit(int amt);
	int withdraw(int amt);
};

