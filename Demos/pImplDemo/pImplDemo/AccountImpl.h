#pragma once
#include "TransactionLog.h"
#include <string>

using std::string;
class Customer;

class AccountImpl
{
private:
	int balance;
	int acctnum;
	static int maxAcctNumUsed;
	TransactionLog log;
	Customer* customer;
	string buildStringForToday();
public:
	AccountImpl(int bal, Customer* cust);
	int deposit(int amt);
	int withdraw(int amt);
};

