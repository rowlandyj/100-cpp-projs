// pImplDemo.cpp : Defines the entry point for the console application.
// Learning how to use pImpl idiom to lower compile times on large projects, especially useful in AGILE environments
// using pImpl is great when the public interface is stable and the private part is volatile

#include "stdafx.h"
#include "Account.h"
#include <vector>

using std::vector;


int _tmain(int argc, _TCHAR* argv[])
{
	Account a1(0, nullptr);
	a1.deposit(50);
	Account a2(0, nullptr);
	a2.deposit(50);
	vector<Account> accounts;
	accounts.push_back(std::move(a1));
	accounts.push_back(std::move(a2));

	return 0;
}

