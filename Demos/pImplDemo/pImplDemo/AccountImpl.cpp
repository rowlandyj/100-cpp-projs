#include "StdAfx.h"
#include "AccountImpl.h"

//must be declared here
int AccountImpl::maxAcctNumUsed;

AccountImpl::AccountImpl(int bal, Customer* cust):
		balance(bal), customer(cust), acctnum(++maxAcctNumUsed), log(acctnum)
{
}


int AccountImpl::deposit(int amt)
{
	log.addDeposit(buildStringForToday(), amt);
	balance += amt;
	return balance;
}

int AccountImpl::withdraw(int amt)
{
	log.addWithdraw(buildStringForToday(), amt);
	balance -= amt;
	return balance;
}

string AccountImpl::buildStringForToday()
{
	return "today";
}
