// comparisonDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Utility.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	int i = 2;

	if (i == 3)
	{
		cout << "i is 3" << endl;
	}
	cout << "i " << i << endl;

	//note how i will be reassigned to 3. = is assignment not comparison
	if (i = 3)
	{
		cout << "i is 3" << endl;
	}
	cout << "i " << i << endl;


	i = 4;
	//this is sometimes called yoda syntax.  Used to avoid mistake above
	if (3 == i)
	{
		cout << "i is 3" << endl;
	}
	cout << "i " << i << endl;

	/*Common C++ Idiom You May See
	int p;
	if (p = foo("whatever"))
	{
		something();
	}

	If the value of p is 0 do something() else do nothing.  
	*/

	cout << "enter a number" << endl;
	cin >> i;

	cout << i << " is ";
	if (!isPrime(i))
	{
		cout << "not ";
	}
	cout << "prime." << endl;

	int j;
	cout << "enter a second number" << endl;
	cin >> j;

	cout << i << " is ";
	//below is same as (!(i % j ==0)) or (i % j != 0)
	//if j is 0, that condition is the same as false
	if (j && (i % j))
	{
		cout << "not ";
	}
	cout << "a multiple of " << j << endl;

	return 0;
}

