// castingDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{

	int i1 = 1;
	std::cout << "i1= " << i1 << std::endl;
	int i2;
	i2 = 2;
	std::cout << "i2= " << i2 << std::endl;
	int i3(3);
	std::cout << "i3= " << i3 << std::endl;

	double d1 = 2.2;
	double d2 = i1;
	//notice how the warning goes away
	int i4 = (int)d1;
	std::cout << "d1= " << d1 << std::endl;
	std::cout << "d2= " << d2 << std::endl;
	std::cout << "i4= " << i4 << std::endl;

	//note chars can use single quote
	char c1 = 'a';
	//double quotes will be a C-style string, there is an end char so an error will come up
	//char c2 = "b";
	std::cout << "c1= " << c1 << std::endl;
	// std::cout << "c2= " << c2 << std::endl;

	bool flag = false;
	std::cout << "flag= " << flag << std::endl;
	//warning will stay; still a performance difference
	flag = (bool)i1;
	std::cout << "flag= " << flag << std::endl;
	flag = (bool)d1;
	std::cout << "flag= " << flag << std::endl;

	//can have a suffix to cast as well.  eg: 1L is a long of 1

	return 0;
}

