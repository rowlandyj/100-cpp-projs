// manualMemory2Demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include <string>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Person Row("Row", "Jiao", 123);
	//we need to call delete in the destructor to avoid mem leaks
	Row.addRes();

	//if addRes() is called again we need to call delete in the fn
	Row.setFirstName("Row2");
	Row.addRes();
	
	//to do the line below we will need to add another constructor
	Person Row2 = Row;

	//finally we will need to overload the assingment operator for below
	Row = Row2;


	return 0;
}

