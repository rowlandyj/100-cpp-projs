#pragma once
#include "Resource.h"

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;
	Resource* pResource;

public:
	Person(string first, string last, int i);
	
	//we need another constructor in case some1 wants to copy a Person
	Person(const Person& p);
	
	~Person();
	string getName() const;
	void setFirstName(string first) {firstName = first;}
	int getId() const {return id;}
	void setId(int i) {id = i;}
	void addRes();
	
	//here is the overload for assignment
	Person& operator=(const Person& p);
};