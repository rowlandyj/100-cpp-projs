#include "stdafx.h"
#include "Person.h"
#include <string>
#include <iostream>

using std::cout;
using std::endl;

//below we have code for showing if something is optional
Person::Person(string first, string last, int i): 
	firstName(first), lastName(last), 
	id(i), pResource(nullptr)
{
}

Person::Person(const Person& p):
	firstName(p.firstName), lastName(p.lastName), 
	id(p.id), pResource(new Resource(p.pResource -> getName()))
{
}

Person::~Person()
{
	//need this line or a memory leak can happen
	delete pResource;
}

string Person::getName() const
{
	return firstName + " " + lastName;
}

void Person::addRes()
{
	/*
		The code below would break because the resource would delete
		right after this is called; r will go out of scope
	*/

	//Resource r("hello");
	//pResource = &r;

	/*
		We need the delete line below or memory leaks may occur
		if this is called multiple times.
		Also remember deleting a nullptr is fine
	*/

	delete pResource; 
	pResource = new Resource("Resource for " + getName());
}

//Just have this like the initialization for the second constructor we made
Person& Person::operator=(const Person& p)
{
	firstName = p.firstName;
	lastName = p.lastName;
	id = p.id;
	//we will need to delete w/e is there before
	delete pResource;
	pResource = new Resource(p.pResource -> getName());
	//'this' is pointer that represents the object itself
	return *this;

}