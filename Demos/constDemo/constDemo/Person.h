#pragma once
#include <string>

using std::string;

class Person
{
private:
	string firstName;
	string lastName;
	int id;

public:
	Person(string first, string last, int i);
	~Person();
	//put the const after the parens
	string getName() const;
	int getId() const {return id;}
	//below will change something so it can't be const
	void setId(int i) {id = i;}
	bool operator<(const Person& p) const;
	bool operator<(int i) const;
	friend bool operator<(int i, const Person& p);
};

bool operator<(int i, const Person& p);