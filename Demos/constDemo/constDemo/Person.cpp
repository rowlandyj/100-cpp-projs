#include "stdafx.h"
#include "Person.h"

Person::Person(string first, string last, int i):
		firstName(first), lastName(last), id(i)
{
}

Person::~Person()
{
}

string Person::getName() const
{
	return firstName + " " + lastName;
}

bool Person::operator<(const Person& p) const
{
	return id < p.id;
}

bool Person::operator<(int i) const
{
	return id < i;
}

bool operator<(int i, const Person& p)
{
	return i < p.id;
}