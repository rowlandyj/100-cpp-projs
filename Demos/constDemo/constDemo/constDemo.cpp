// constDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"

int doubleInt(const int& x)
{
	return x * 2;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int i = 3;
	const int ci = 3;
	i = 4;
	//const variables aren't modifiable
	//ci = 4

	int j = 10;
	int doubleJ = doubleInt(j);
	//the below line works because of the use of (const int&)
	//if it was just (int&) an error would be thrown b/c 10 is not moddable
	int doubleTen = doubleInt(10);

	Person row("Row", "Jiao", 123);
	row.setId(456);
	const Person cRow = row;
	//again you can't modify a const
	//cRow.setId(789);
	//for the below line to work we need to add const after member fn declaration
	int rowId = cRow.getId();

	/*
		const can also work with pointers
		you can have a const pointer that cant point elsewhere
				int * const cpI
		you can also point to a const
				const int* cpI
		you can also do both:
				const int * const cpI
	*/

	int* pI = &i;
	//int* pII = &ci; does not work
	const int* pII = &ci;

	Person* pRow = &row;
	Person blah("blah", "bleh", 789);

	const int* pcI = pI; //pointer to a const
	//*pcI = 4;
	pcI = &j; //j = 10, points at something else now

	int* const cpI = pI;
	*cpI = 4;
	//cpI = &j;  const pointer can't point at something else

	const int* const crazy = pI; //cant point at something else or change value
	//*crazy = 4;
	//crazy = &j;
	//we can see what crazy's value is though
	int crazValue = *crazy;

	return 0;
}

