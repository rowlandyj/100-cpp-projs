#include "StdAfx.h"
#include "Person.h"


Person::Person(string first, string last, int i):
	firstName(first), lastName(last), id(i)
{
}


Person::~Person(void)
{
}

//we dont need to add virtual here, in the header is enough
string Person::getName() const
{
	return firstName + " " + lastName;
}
