#pragma once
#include <string>

using std::string;

class Resource
{
private:
	string name;
public:
	Resource(string n);
	~Resource(void);
	virtual string getName() const {return name;}
};

