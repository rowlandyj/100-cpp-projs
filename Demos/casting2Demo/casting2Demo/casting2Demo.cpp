// casting2Demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Person.h"
#include "Tweeter.h"
#include <iostream>
#include "Resource.h"

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Tweeter t("Row", "Jiao", 123, "@RJ");
	Person* p = &t;
	//We can use round bracket cast but it can go badly
	//eg we can round bracket cast (Tweeter*) to a int
	Tweeter* pt = static_cast<Tweeter*>(p);
	cout << pt -> getName() << endl;

	Resource f("local");
	//if we ran this without the if statement we would get an error
	Tweeter* pi = dynamic_cast<Tweeter*>(&f);
	//when dynamc_cast fails we get a nullptr
	if (pi)
	{
		cout << pi -> getName() << endl;
	}
	else
	{
		cout << "Resource can't be passed to Tweeter" << endl;
	}


	return 0;
}

